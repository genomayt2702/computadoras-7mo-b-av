import java.util.Scanner;
public class Ejercicioif4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String cat;
		System.out.println("Seleccione una de las siguientes categorias \'a\', \'b\' o \'c\'");
		cat = s.next();
		if(cat.equalsIgnoreCase("a")){
			System.out.println("Hijo");
		}else if(cat.equalsIgnoreCase("b")){
			System.out.println("Padres");
		}else if(cat.equalsIgnoreCase("c")){
			System.out.println("Abuelos");
		}else{
			System.out.println("Esa categoria no existe");
		}

	}

}
