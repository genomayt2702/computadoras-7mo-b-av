import java.util.Scanner;

public class Ejercicioif3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String a;
		System.out.println("El mes tiene que estar escrito completamente por letras mayusculas sin espacios al principio, al final o entre los caracteres\nIngrese un mes");
		a = s.next();
		if(a.equalsIgnoreCase("ENERO")){
			System.out.println(a + " tiene 31 dias");
		}else if(a.equalsIgnoreCase("FEBRERO")){
			System.out.println(a + " tiene 28 dias en anos naturales y 29 en anos bisiestos");
		}else if(a.equalsIgnoreCase("MARZO")){
			System.out.println(a + " tiene 31 dias");
		}else if(a.equalsIgnoreCase("ABRIL")){
			System.out.println(a + " tiene 30 dias");
		}else if(a.equalsIgnoreCase("MAYO")){
			System.out.println(a + " 31 tiene dias");
		}else if(a.equalsIgnoreCase("JUNIO")){
			System.out.println(a + " tiene 30 dias");
		}else if(a.equalsIgnoreCase("JULIO")){
			System.out.println(a + " tiene 31 dias");
		}else if(a.equalsIgnoreCase("AGOSTO")){
			System.out.println(a + " tiene 31 dias");
		}else if(a.equalsIgnoreCase("SEPTIEMBRE")){
			System.out.println(a + " tiene 30 dias");
		}else if(a.equalsIgnoreCase("OCTUBRE")){
			System.out.println(a + " tiene 31 dias");	
		}else if(a.equalsIgnoreCase("NOVIEMBRE")){
			System.out.println(a + " tiene 31 dias");
		}else if(a.equalsIgnoreCase("DICIEMBRE")){
			System.out.println(a + " tiene 31 dias");
		}else{
			System.out.println("El mes mencionado no cumple con los parametros preestablecidos, esta mal escrito o directamente no existe");
		}

	}

}
