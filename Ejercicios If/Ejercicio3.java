
public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Tecla de escape\t\tSignificado");
		System.out.println("\\n\t\t\tSignifica nueva linea.");
		System.out.println("\\t\t\t\tSignifica un tab de espacio.");
		System.out.println("\\\"\t\t\tEs para poner \"(comillas dobles)dentro del texto, por ejemplo, \"Belencita\". ");
		System.out.println("\\\\\t\t\tSe utiliza para poner \\ dentro del texto, por ejemplo, \\algo\\.");
		System.out.println("\\\'\t\t\tSe utiliza para \'(comilla simple) dentro del texto, por ejemplo, \'Princesita\'.");
	}

}
