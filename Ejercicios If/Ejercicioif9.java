import java.util.Scanner;
public class Ejercicioif9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		int P1, P2;
		System.out.println("NOTA:\n> 0 = PIEDRA\n> 1 = PAPEL\n> 2 = TIJERAS\nTurno del Jugador 1:");
		P1 = s.nextInt();
		System.out.println("NOTA:\n> 0 = PIEDRA\n> 1 = PAPEL\n> 2 = TIJERAS\nTurno del Jugador 2:");
		P2 = s.nextInt();
		if(P1>=0 && P1<=2 && P2>=0 && P2<=2){
			if(P1==0){
				if(P2==0){
					System.out.println("Empate");
				}else if(P2==1){
					System.out.println("Gana el Jugador 2");
				}else{
					System.out.println("Gana el Jugador 1");
				}
			}else if(P1==1){
				if(P2==0){
					System.out.println("Gana el Jugador 1");
				}else if(P2==1){
					System.out.println("Empate");
				}else{
					System.out.println("Gana el Jugador 2");
				}
			}else if(P2==0){
				System.out.println("Gana el Jugador 2");
			}else if(P2==1){
				System.out.println("Gana el Jugador 1");
			}else{
				System.out.println("Empate");
			}
		}else{
			System.out.println("La instruccion no fue correctamente ingresada");
		}


	}

}
