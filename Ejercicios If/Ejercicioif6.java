import java.util.Scanner;
public class Ejercicioif6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		int a;
		System.out.println("Ingrese la edad");
		a = s.nextInt();
		if(a==0){
			System.out.println("Se encuentra en Jardin de infantes");
		}else if(a>=1 && a<=6){
			System.out.println("Se encuentra en Primaria");
		}else if(a>=7 && a<=12){
			System.out.println("Se encuentra en Secundaria");
		}else{
			System.out.println("Esta edad no esta entre los rangos preestablecidos");
		}

	}

}
