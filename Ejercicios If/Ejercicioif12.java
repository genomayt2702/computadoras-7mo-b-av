import java.util.Scanner;
public class Ejercicioif12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		int a;
		System.out.println("Ingrese un valor:");
		a = s.nextInt();
		if(a>=1 && a<=36){
			if(a<=12){
				System.out.println("El valor " + a + " pertenece a la primer docena");
			}else if(a<=24){
				System.out.println("El valor " + a + " pertenece a la segunda docena");
			}else{
				System.out.println("El valor " + a + " pertenece a la tercer docena");
			}
		}else{
			System.out.println("El valor:" + a + " esta fuera del rango");
		}

	}

}
