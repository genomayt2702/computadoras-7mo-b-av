import java.util.Scanner;
public class Ejercicioswitch13 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String mes;
		int x;
		System.out.println("Ingrese un mes:");
		mes = s.next();
		if(mes.equalsIgnoreCase("FEBRERO")){
			x = 0;
		}else if(mes.equalsIgnoreCase("ABRIL") || mes.equalsIgnoreCase("JUNIO") || mes.equalsIgnoreCase("SEPTIEMBRE") || mes.equalsIgnoreCase("NOVIEMBRE")){
			x = 1;
		}else if(mes.equalsIgnoreCase("ENERO") || mes.equalsIgnoreCase("MARZO") || mes.equalsIgnoreCase("MAYO") || mes.equalsIgnoreCase("JULIO") || mes.equalsIgnoreCase("AGOSTO") || mes.equalsIgnoreCase("OCTUBRE") || mes.equalsIgnoreCase("DICIEMBRE")){
			x = 2;
		}else{
			x = 3;
		}
		
		switch(x){
		case 0:
		{
			System.out.println(mes + " tiene 28 dias");
			break;
		}
		case 1:
		{
			System.out.println(mes + " tiene 30 dias");
			break;
		}
		case 2:
		{
			System.out.println(mes + " tiene 31 dias");
			break;
		}
		default:
		{
			System.out.println("El mes esta mal escrito o directamente no existe");
		}
		}

	}

}
