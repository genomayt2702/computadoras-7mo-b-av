package objetos.controller.composite.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetos.modelo.Circulo;
import objetos.controller.composite.ValidatorComposite;
import objetos.modelo.Figura;



public class ValidatorCompositeTest {
	Circulo figTest;
	
	@Before
	public void setUp() throws Exception {
		figTest = new Circulo("cir  doble espacio", -10);
	}

	@After
	public void tearDown() throws Exception {
		figTest=null;
	}

	@Test
	public void testGetErroresNombreDobleEspacioyRadioNegativo() {
		assertEquals("El nombre no puede tener dos espacios\nEl radio debe ser mayor que 0 (cero)\n", ValidatorComposite.getErrores(figTest));
	}
	
	@Test
	public void testGetErroresNombreCirculoDobleEspacio() {
		figTest.setNombre("circulo");
		assertEquals("El nombre no puede tener dos espacios\n", ValidatorComposite.getErrores(figTest));
	}
	
	@Test
	public void testGetErroresRadioNegativo() {
		figTest.setRadio(20);
		assertEquals("El radio debe ser mayor que 0 (cero)\n", ValidatorComposite.getErrores(figTest));
	}
	
	
}



	
