package objetos.controller.composite;

import java.util.ArrayList;
import java.util.List;

import objetos.modelo.Figura;

/**
 * Esta clase agrupa todas las validaciones, es un patron composite por lo cual el padre conoce a todos sus hijos 
 * y devuelve la acumulacion de todos los errores
 * @author Alejo Giunta
 *
 */


public abstract class ValidatorComposite {
	protected static Figura figura;
	
	public ValidatorComposite() {	}
	
	public static String getErrores(Figura fig){
		figura=fig;
		
		List<ValidatorComposite> validaciones = new ArrayList<>();
		
		validaciones.add(new FiguraNombreDobleEspacioComposite());
		validaciones.add(new FiguraNombreVacioComposite());
		validaciones.add(new CirculoRadioNegativoCeroComposite());
		validaciones.add(new CuadradoLadoNegativoCeroComposite());
		validaciones.add(new RectanguloBaseNegativoCeroComposite());
		validaciones.add(new RectanguloAlturaNegativoCeroComposite());
		
		StringBuffer sbErrorers = new StringBuffer();
		for (ValidatorComposite validacion : validaciones) {
			if(validacion.isMe() && validacion.validar()){
				sbErrorers.append(validacion.getError());
				sbErrorers.append("\n");
			}
		}
		return sbErrorers.toString();
	}
	
	/**
	 * Este metodo devuelve true para el caso de que la validacion corresponda
	 * @return true si corresponde y false si no corresponde
	 *
	 */

	public abstract boolean isMe();
	/**
	 * validar devuelve verdadero para rl caso
	 * @return true si se da el error y false si no se da
	 *
	 */
	public abstract boolean validar();
	/**
	 * Devuelve el texto del error que le corresponde al validar
	 * @return texto con error
	 *
	 */
	public abstract String getError();
	
	

}
