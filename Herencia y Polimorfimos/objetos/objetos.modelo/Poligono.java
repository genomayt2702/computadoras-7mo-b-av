package objetos.modelo;

public class Poligono extends Figura {
	private float apotema;
	private float cantidadDeLados;
	private float lado;
	
	public Poligono() {}

	public Poligono(String pNombre) {
		super(pNombre);
	}

	
	public Poligono(String pNombre, float pApotema, float pCantidadDeLados, float pLado) {
		this(pNombre);
		this.apotema = pApotema;
		this.cantidadDeLados = pCantidadDeLados;
		this.lado = pLado;
		Figura.maximaSuperficie = Math.max(Figura.maximaSuperficie, calcularSuperficie());
	}

	public float getApotema() {			return apotema;		}
	public void setApotema(float pApotema) {	this.apotema = pApotema;	}

	public float getCantidadDeLados() {				return cantidadDeLados;			}
	public void setCantidadDeLados(float pCantidadDeLados) {	this.cantidadDeLados = pCantidadDeLados;	}
	
	public float getLado() {				return lado;			}
	public void setLado(float pLado) {	this.lado = pLado;	}
	

	@Override
		public float calcularPerimetro() {
			return  lado*cantidadDeLados; 

	}

	@Override
	public float calcularSuperficie() {
		return lado*cantidadDeLados*apotema/2;
	}

	@Override
	public String getValores() {
		StringBuffer sb = new StringBuffer("a=");
		sb.append(apotema);
		sb.append("l=");
		sb.append(lado);
		sb.append("n=");
		sb.append(cantidadDeLados);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(apotema);
		result = prime * result + Float.floatToIntBits(lado);
		result = prime * result + Float.floatToIntBits(cantidadDeLados);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Poligono)) {
			return false;
		}
		Poligono other = (Poligono) obj;
		if (Float.floatToIntBits(apotema) != Float.floatToIntBits(other.apotema)) {
			return false;
		}
		if (Float.floatToIntBits(lado) != Float.floatToIntBits(other.lado)) {
			return false;
		}
		if (Float.floatToIntBits(cantidadDeLados) != Float.floatToIntBits(other.cantidadDeLados)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + "\napotema=" + apotema + "\nlado=" + lado + "\ncantidadDeLados" + cantidadDeLados;
	}



}
