

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio1 {

	private JFrame frame;
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txt3;
	private JLabel lblPromedio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 window = new Ejercicio1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLUE);
		frame.setBounds(100, 100, 685, 391);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNota = new JLabel("Nota 1 ");
		lblNota.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblNota.setBounds(30, 63, 65, 25);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_1 = new JLabel("Nota 2");
		lblNota_1.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblNota_1.setBounds(30, 133, 65, 25);
		frame.getContentPane().add(lblNota_1);
		
		JLabel lblNota_2 = new JLabel("Nota 3");
		lblNota_2.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblNota_2.setBounds(30, 198, 65, 33);
		frame.getContentPane().add(lblNota_2);
		
		txt1 = new JTextField();
		txt1.setBounds(117, 67, 116, 22);
		frame.getContentPane().add(txt1);
		txt1.setColumns(10);
		
		txt2 = new JTextField();
		txt2.setText("");
		txt2.setBounds(117, 137, 116, 22);
		frame.getContentPane().add(txt2);
		txt2.setColumns(10);
		
		txt3 = new JTextField();
		txt3.setBounds(117, 206, 116, 22);
		frame.getContentPane().add(txt3);
		txt3.setColumns(10);
		
		JButton btnCalcularPromedio = new JButton("Calcular Promedio");
		btnCalcularPromedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				float fnota1 = Float.parseFloat(txt1.getText());
				float fnota2 = Float.parseFloat(txt2.getText());
				float fnota3 = Float.parseFloat(txt3.getText());
				
			float fpromedio = (fnota1 + fnota2 + fnota3)/3;
			
			if (fpromedio>=7) {
				lblPromedio.setText("Aprobado") ;
			}
				else lblPromedio.setText("Desaprobado");{
			}
				
				
				
			}
		});
		btnCalcularPromedio.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnCalcularPromedio.setForeground(new Color(0, 0, 0));
		btnCalcularPromedio.setBounds(315, 196, 205, 42);
		frame.getContentPane().add(btnCalcularPromedio);
		
		lblPromedio = new JLabel("Promedio");
		lblPromedio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPromedio.setOpaque(true);
		lblPromedio.setBackground(Color.GREEN);
		lblPromedio.setForeground(new Color(0, 0, 0));
		lblPromedio.setBounds(361, 130, 124, 33);
		frame.getContentPane().add(lblPromedio);
	}
}
