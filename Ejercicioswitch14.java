import java.util.Scanner;
public class Ejercicioswitch14 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		int a;
		System.out.println("Ingrese el puesto en el que se encuentra:");
		a = s.nextInt();
		switch(a){
		case 1:
		{
			System.out.println("El puesto numero:" +a + " gana la medalla de oro");
			break;
		}
		case 2: 
		{
			System.out.println("El puesto numero:" +a + " gana la medalla de plata");
			break;
		}
		case 3: 
		{
			System.out.println("El puesto numero:" +a + " gana la medalla de bronce");
			break;
		}
		default:
		{
			System.out.println("El puesto numero:" +a + " no entra en el podio, siga participando");
			break;
		}
		}

	}

}
