import java.util.Scanner;
public class Ejercicioswitch15 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String clase;
		int x;
		clase = s.next();
		if(clase.equalsIgnoreCase("a")){
			x = 0;
		}else if(clase.equalsIgnoreCase("b")){
			x = 1;
		}else if(clase.equalsIgnoreCase("c")){
			x = 2;
		}else{
			x = 3;
		}
		switch(x){
		case 0:
		{
			System.out.println("Los autos clase \'" + clase + "\'" + " tienen 4 ruedas y un motor.");
			break;
		}
		case 1:
		{
			System.out.println("Los autos clase \'" +clase + "\'" + " tienen 4 ruedas, un motor, cierre centralizado y aire condicionado.");
			break;
		}
		case 2: 
		{
			System.out.println("Los autos clase \'" + clase + "\'" + " tienen 4 ruedas, un motor, cierre centralizado, aire condicionado y airbag.");
			break;
		}
		default:
		{
			System.out.println("Los autos clase \'" + clase + "\'" + " no existe.");
			break;
		}
		}

	}

}
